<!doctype html>
<html lang="es">
  <head>
   <?php require "../app/views/parts/head.php" ?>
  </head>
  <body>

<?php require "../app/views/parts/header.php" ?>

    <main role="main" class="container">
     <br><br><br><br>
      <div class="starter-template">
        <h1>Alta de Jugadores</h1>
        <form method="post" action="/jugadores/store">
            <div class="form-group">
             <label for="text">Nombre :</label>
             <input type="text" name="nombre" class="form-control" >
             </div>

             <div class="form-group">
            <label for="text">nacimiento :</label>
            <input type="DATETIME" name="nacimiento" class="form-control" >
            </div>

            <div class="form-group">
             <label for="text">id_puesto :</label>
              <select name="id_puesto">
            <?php foreach($jugadores as $jugador ) { ?>
                <option value="<?php echo $jugador->id?>">
                  <?php echo $jugador->puesto()->nombre ?>
                </option>
            <?php } ?>
          </select>
            </div>


            <button type="submit" class="btn btn-default">Enviar</button>
  </div>
        </form>
          <a href="/jugadores">Volver a jugadores</a>
      </div>
    </main><!-- /.container -->
        <?php require "../app/views/parts/footer.php" ?>
</body>
        <?php require "../app/views/parts/scripts.php" ?>
</html>
