<!doctype html>
<html lang="en">
  <head>
   <?php require "../app/views/parts/head.php" ?>
  </head>
  <body>

<?php require "../app/views/parts/header.php" ?>

    <main role="main" class="container">
     <br><br><br><br>
      <div class="starter-template">
        <h1>Jugadores </h1>
        <table class="table table-striped">
          <thead>
            <tr>
            <th>Id</th>
            <th>Nombre</th>
            <th>Puesto</th>
            <th>Nacimiento</th>
            <th>Operaciones</th>
            </tr>
           </thead>
          <tbody>
          <?php foreach ($jugador as $jugador): ?>
            <tr>
                 <td><?php echo $jugador->id ?></td>
                <td><?php echo $jugador->nombre?></td>
                <td><?php echo $jugador->puestos()->nombre?></td>
                <td><?php echo $jugador->datetime ?></td>
            <td><a href="/jugadores/edit/<?php echo $jugador->id ?>">actualizar</a></td>
            </tr>
          <?php endforeach ?>
          </tbody>
        </table>

          <a href="/jugadores/create">Nuevo jugador</a>
      </div>

    </main><!-- /.container -->
<?php require "../app/views/parts/footer.php" ?>
 </body>
 <?php require "../app/views/parts/scripts.php" ?>
</html>

