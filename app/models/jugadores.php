<?php
namespace App\Models;

use PDO;
use Core\Model;

/**
*
*/
class jugadores extends Model
{

    function __construct()
    {

    }

    public static function all(){
    $db = jugadores::db();
    $statement = $db->query('SELECT * FROM jugadores');
    $jugador = $statement->fetchAll(PDO::FETCH_CLASS,jugadores::class);
    return $jugador;
}




    public static function find($id){
    $db = jugadores::db();
    $statemet = $db->prepare('SELECT * FROM jugadores WHERE id=?');
    $statemet->bindValue(1, $id, PDO::PARAM_INT);
    $statemet->execute();
    $statemet-> setFetchMode(PDO::FETCH_CLASS,jugadores::class);
    $jugador = $statemet->fetchAll(PDO::FETCH_CLASS);
    return $jugador[0];
    }

    public function puestos(){
       //un producto pertenece a un tipo:
     $db = jugadores::db();
     $statement = $db->prepare('SELECT * FROM puestos WHERE nombre = :nombre');
     $statement->bindValue(':nombre', $this->puestos);
     $statement->execute();
     $jugador = $statement->fetchAll(PDO::FETCH_CLASS, puestos::class)[0];

       return $jugador;
   }


    public function insert(){

    $db = jugadores::db();
    $statemet = $db->prepare('INSERT INTO jugadores(name,DATETIME,id_puesto) VALUES (:name , :DATETIME,:id_puesto )');

    $statemet->bindValue(':name', $this->name);
    $statemet->bindValue(':DATETIME', $this->DATETIME);
    $statemet->bindValue(':id_puesto', $this->id_puesto);

    return $statemet->execute(); //devuelve el num de filas

    }


public function paginate($size =10){
    if(isset($_REQUEST['page'])){
    $page = (integer) $_REQUEST['page'];

    }else{
        $page=1;
    }

    $offset =($page -1) * $size;

    $db = jugadores::db();
    $statement = $db->prepare('SELECT * FROM jugadores LIMIT :pagesize OFFSET :offset');
    $statement->bindValue('pagesize',$size, PDO::PARAM_INT);
    $statement->bindValue('offset',$offset, PDO::PARAM_INT);
    $statement->execute();

    $users = $statement->fetchAll(PDO::FETCH_CLASS,jugadores::class);
    return $jugador;

    }

    public static function rowCount(){
    $db = jugadores::db();
    $statement = $db->prepare('SELECT count(id) as count FROM jugadores');
    $statement->execute();

        $rowCount = $statement->fetch(PDO::FETCH_ASSOC);
        return $rowCount['count'];

    }

}
