<?php
namespace App\Models;

use PDO;
use Core\Model;

// require_once '../core/Model.php';
/**
*
*/
class User extends Model
{

    function __construct()
    {

    }


public function paginate($size =10){
    if(isset($_REQUEST['page'])){
    $page = (integer) $_REQUEST['page'];

    }else{
        $page=1;
    }

    $offset =($page -1) * $size;

$db = User::db();
$statement = $db->prepare('SELECT * FROM jugadores LIMIT :pagesize OFFSET :offset');
$statement->bindValue('pagesize',$size, PDO::PARAM_INT);
$statement->bindValue('offset',$offset, PDO::PARAM_INT);
$statement->execute();

$jugadores = $statement->fetchAll(PDO::FETCH_CLASS,User::class);
return $jugadores;

}

public static function rowCount(){
$db = User::db();
$statement = $db->prepare('SELECT count(id) as count FROM jugadores');
$statement->execute();

$rowCount = $statement->fetch(PDO::FETCH_ASSOC);
return $rowCount['count'];

}
}
